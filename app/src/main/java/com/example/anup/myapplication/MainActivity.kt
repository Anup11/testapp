package com.example.anup.myapplication

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.MotionEventCompat
import android.util.Log
import android.view.MotionEvent
import android.view.MotionEvent.actionToString

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }


    override fun onTouchEvent(event: MotionEvent?): Boolean {

        val (xPos: Int, yPos: Int) = MotionEventCompat.getActionMasked(event).let { action ->
            Log.d(localClassName, "The action is ${actionToString(action)}")
            // Get the index of the pointer associated with the action.
            MotionEventCompat.getActionIndex(event).let { index ->
                // The coordinates of the current screen contact, relative to
                // the responding View or Activity.
                MotionEventCompat.getX(event, index).toInt() to MotionEventCompat.getY(event, index).toInt()
            }
        }

        if (event!=null && event?.pointerCount > 1) {
            Log.d(localClassName, "Multitouch event")

        } else {
            // Single touch event
            Log.d(localClassName, "Single touch event")
        }

        return super.onTouchEvent(event)
    }

    // Given an action int, returns a string description
    fun actionToString(action: Int): String {
        return when (action) {
            MotionEvent.ACTION_DOWN -> "Down"
            MotionEvent.ACTION_MOVE -> "Move"
            MotionEvent.ACTION_POINTER_DOWN -> "Pointer Down"
            MotionEvent.ACTION_UP -> "Up"
            MotionEvent.ACTION_POINTER_UP -> "Pointer Up"
            MotionEvent.ACTION_OUTSIDE -> "Outside"
            MotionEvent.ACTION_CANCEL -> "Cancel"
            else -> ""
        }
    }
}
